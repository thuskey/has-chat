import { createRouter, createWebHistory, createWebHashHistory } from "vue-router";

import Home from "../views/Home/Home.vue";

const routes = [
  { path: "/", redirect: { name: "Home" }, },
  { path: "/haschat", name: "Home", component: Home },
  { path: "/haschat/main", name: "Main", component: () => import("@/views/HasChat/HasChat.vue") }
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;